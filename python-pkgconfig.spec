%global _empty_manifest_terminate_build 0
Name:		python-pkgconfig
Version:	1.5.5
Release:	1
Summary:	A Python module to interface with the pkg-config command line tool 
License:	MIT
URL:		https://github.com/matze/pkgconfig
Source0:	https://files.pythonhosted.org/packages/c4/e0/e05fee8b5425db6f83237128742e7e5ef26219b687ab8f0d41ed0422125e/pkgconfig-1.5.5.tar.gz
BuildArch:	noarch

%description
pkgconfig is a Python module to interface with the pkg-config command line tool and supports Python 3.3+.

%package -n python3-pkgconfig
Summary:	A Python module to interface with the pkg-config command line tool 
Provides:	python-pkgconfig = %{version}-%{release}
Requires:	pkgconf-pkg-config
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-pkgconfig
pkgconfig is a Python module to interface with the pkg-config command line tool and supports Python 3.3+.

%package help
Summary:	Development documents and examples for pkgconfig
Provides:	python3-pkgconfig-doc
%description help
pkgconfig is a Python module to interface with the pkg-config command line tool and supports Python 3.3+.

%prep
%autosetup -n pkgconfig-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pkgconfig -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Nov 23 2022 wangjunqi <wangjunqi@kylinos.cn> - 1.5.5-1
- Update package to version 1.5.5

* Wed Oct 21 2020 wangxiao <wangxiao65@huawei.com> - 1.5.1-2
- drop python2 subpackage

* Thu Sep 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.5.1-1
- Package Init
